package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ColorlibFormValidationPage extends PageObject{
	
	@FindBy(id="req")
	public WebElementFacade txtRequired;
	
	@FindBy(id="sport")
	public WebElementFacade cmbSport1;
	
	@FindBy(name="sport2")
	public WebElementFacade cmbSport2;
	
	@FindBy(id="url1")
	public WebElementFacade txtUrl;
	
	@FindBy(id="email1")
	public WebElementFacade txtEmail1;
	
	@FindBy(id="pass1")
	public WebElementFacade txtPass1;
	
	@FindBy(id="pass2")
	public WebElementFacade txtPass2;
	
	@FindBy(id="minsize1")
	public WebElementFacade txtMinsize1;
	
	@FindBy(id="maxsize1")
	public WebElementFacade txtMaxsize1;
	
	@FindBy(id="number2")
	public WebElementFacade txtNumber;
	
	@FindBy(id="ip")
	public WebElementFacade txtIp;
	
	@FindBy(id="date3")
	public WebElementFacade txtDate;
	
	@FindBy(id="past")
	public WebElementFacade txtDateEarlier;
	
	//Boton validate
	@FindBy(xpath="//*[@id='popup-validation']/div[14]/input")
	public WebElementFacade btnValidate;
	
	//Globo informativo
	@FindBy(xpath="//DIV[@class='formErrorContent']")
	public WebElementFacade globoInformativo;
	
	
	public void DiligenciarCampo(WebElementFacade Campo, String dato)
	{
		Campo.click();
		Campo.clear();
		Campo.sendKeys(dato);
	}
	
	public void Select_Sport(String dato)
	{
		cmbSport1.click();
		cmbSport1.selectByVisibleText(dato);
	}
	
	
	public void Multiple_Select(String dato)
	{
		cmbSport2.selectByVisibleText(dato);
	}
	
	public void validate()
	{
		btnValidate.click();
	}
	
	public void form_sin_errores()
	{
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}
	
	public void form_con_errores()
	{
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}

}
