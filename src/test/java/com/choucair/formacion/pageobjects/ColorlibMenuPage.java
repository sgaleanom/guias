package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.PageObjects;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat; 

public class ColorlibMenuPage extends PageObject{
	
	//Menu Forms
	@FindBy(xpath="//*[@id='menu']/li[6]/a")
	public WebElementFacade menuForms;
	
	//sub menu forms General
	@FindBy(xpath="//*[@id='menu']/li[6]/ul/li[1]/a")
	public WebElementFacade menuFormsGenerals;
	
	//sub menu forms validation
	@FindBy(xpath="//*[@id='menu']/li[6]/ul/li[2]/a")
	public WebElementFacade menuFormsValidation;
	
	//label PopupValidation
	@FindBy(xpath="//*[@id='content']/div/div/div[1]/div/div/header/h5")
	public WebElementFacade lblFormValidation;
	
	public void menuFormValidation()
	{
		menuForms.click();
		menuFormsValidation.click();
		String strMensaje = lblFormValidation.getText();
		assertThat(strMensaje, containsString("Popup Validation"));
	}

}
