package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ColorlibFormValidationPage;

import net.thucydides.core.annotations.Step;

public class ColorlibFormValidationSteps {
	
	ColorlibFormValidationPage colorlibFormValidationPage;
	
	@Step
	public void diligenciar_popup_datos_tabla(List<List<String>> data, int id)
	{
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtRequired, data.get(id).get(0).trim());
		colorlibFormValidationPage.Select_Sport(data.get(id).get(1).trim());
		colorlibFormValidationPage.Multiple_Select(data.get(id).get(2).trim());
		colorlibFormValidationPage.Multiple_Select(data.get(id).get(3).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtUrl, data.get(id).get(4).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtEmail1, data.get(id).get(5).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtPass1, data.get(id).get(6).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtPass2, data.get(id).get(7).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtMinsize1, data.get(id).get(8).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtMaxsize1, data.get(id).get(9).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtNumber, data.get(id).get(10).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtIp, data.get(id).get(11).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtDate, data.get(id).get(12).trim());
		colorlibFormValidationPage.DiligenciarCampo(colorlibFormValidationPage.txtDateEarlier, data.get(id).get(13).trim());
		colorlibFormValidationPage.validate();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_exitoso()
	{
		colorlibFormValidationPage.form_sin_errores();
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_con_errores()
	{
		colorlibFormValidationPage.form_con_errores();
	}
	

}
