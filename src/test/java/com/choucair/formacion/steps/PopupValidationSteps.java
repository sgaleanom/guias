package com.choucair.formacion.steps;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {
	
	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;
	
	
	@Step
	public void login_colorlib(String strUsuario, String strPass)
	{
//		a. Abrir navegador con la url de prueba
		colorlibLoginPage.open();
//		b. Ingresar usuario demo
//		c. Ingresar password demo
//		d. Click en botón Sign in
		colorlibLoginPage.IngresarDatos(strUsuario, strPass);
//		e. Verificar la Autenticación (label en home)
		colorlibLoginPage.VerificaHome();
	}
	
	//Forma1
	@Step
	public void ingresar_form_validation()
	{
		colorlibMenuPage.menuFormValidation();
	}
	/*Forma 2
	 * @Step
	 * public void ingresar_form_validation()
	{
		menuForms.click();
		menuFormsValidation.click();
		String strMensaje = lblFormValidation.getText();
		assertThat(strMensaje, containsString("Popup Validation"));
	}
	 */
	
	


}
